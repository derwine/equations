import time
import curses

print "Plotting your graph"
time.sleep(1)

#arrays with graph coordinates
x_coords = [5,4,3,2,1]
y_coords = [22,18,14,10,7]

#method to take coordintes and raw the plot points on the screen
def plot_graph(window):

		for i in range(len(x_coords)):			
			window.addstr(x_coords[i], y_coords[i], ".")
			window.refresh()
			time.sleep(0.5)
		
			time.sleep(2)

#use curses library to handles drawing method.
curses.wrapper(plot_graph)

