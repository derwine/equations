"""                            Simple Equation Solver V.1 by Derwin Emmanuel
-------------------------------------------------------------------------------------------------
a. Takes linear and quadratic equations and print out results for a range of values.
b. These values are then ploted to a graph using the pygame library
--plot function still needs work
-------------------------------------------------------------------------------------------------
"""
class linearEq(object):

    #set the default range for the plot
    def __init__(self):
        #set the initial range
        self.r1 = 5
        #set the negative range
        self.r2 = -self.r1
        #create 2 empty lists to hold coordinates
        self.x_set = [ ]
        self.y_set = [ ]

    #simple substitution
    def solve_for_x(self, a, c, x):
        #solve the equation
        y = a*x + c
        #return the answer
        return y

   ## This needs some more work D.E. 9.9.2012
    def plot(self, a, c):


        while (self.r1 >= self.r2):
                x_coord = self.r1
                y_coord = self.solve_for_x(a, c, x_coord)

                self.x_set.append(x_coord)
                self.y_set.append(y_coord)

                #adjust
                self.r1 = self.r1 - 1

        return self.x_set, self.y_set
