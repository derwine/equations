from equations import *
from sys import exit

def solve_one(a, c):
    while True:
        try:
            x = float(raw_input('Good! Now, Lets test a value for X:'))
            break
        except ValueError as e:
            print "There was an Error %s" %e

    eq1 = linearEq()

    result =  eq1.solve_for_x(a,c,x)

    print "----------------------------------------------------------------------"
    print "The solution for Y = %dx + %d, where X = %d is: %d \n" %(a,c,x,result)


def plot_one(a, c):

    eq1 = linearEq()
    print "----------------------------------------------------------------------"
    x_cdnts, y_cdnts =  eq1.plot(a, c)

    #print the results
    print "Great!: The Equation is now Y = %ix + %i \n" %(a,c)
    print "X co-ordinates are: ", x_cdnts
    print "Y co-ordinates are: ", y_cdnts

    print "\nPoints to plot are:"

    i = 0
    while i < len(x_cdnts):

        print "(x %i, y %i)" %(x_cdnts[i] , y_cdnts[i])
        i = i + 1
        #print "\t"



print "-----------------------------------------------------------------------------\n"
## Try one out
print "We are going to solve a linear equation"
print "The form is Y = Ax + C   e.g. Y = 2x + 3"

#get value
while True:
    try:
        a = float(raw_input('Enter the value of A:'))
        break
    except ValueError as e:
        print "There was an Error: \"%s\"" %e


while True:
    try:
        c = float(raw_input('Enter the value of C:'))
        break
    except ValueError as e:
        print "There was an Error %s" %e

print "Great!: The Equation is now Y = %ix + %i \n" %(a,c)

print "----------------------------------------------------------"
print "What do you want to do: "

#have user decide on what to do
while True:
    print "To solve the equation Press [1] "
    print "To Plot the equation  Press [2]"
    try:
        choice = int(raw_input(' >:'))
    
        if choice == 1:
            solve_one(a, c)
            break
        elif choice == 2:
            plot_one(a, c)
            break
        else:
            print "Invalid option try again! or press [Ctrl + D] to exit"
    except ValueError as e:
        print "Oops! You did not enter a number. Try again!"       

